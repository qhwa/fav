defmodule Fav.Favourites.Favourite do
  use Ecto.Schema
  import Ecto.Changeset

  schema "favourites" do
    belongs_to :movie, Fav.Movies.Movie
    belongs_to :user, Fav.Users.User

    timestamps()
  end

  @doc false
  def changeset(favourite, attrs) do
    favourite
    |> cast(attrs, [])
    |> validate_required([])
  end
end
