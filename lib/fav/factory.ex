defmodule Fav.Factory do
  use ExMachina.Ecto, repo: Fav.Repo

  alias Fav.{
    Favourites.Favourite,
    Movies.Movie,
    Users.User
  }

  def user_factory do
    %User{name: sequence(:name, &"user-#{&1}")}
  end

  def movie_factory do
    %Movie{name: sequence(:name, &"movie-#{&1}")}
  end

  def favourite_factory do
    %Favourite{
      user: build(:user),
      movie: build(:movie)
    }
  end
end
