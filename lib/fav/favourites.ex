defmodule Fav.Favourites do
  @moduledoc """
  The Favourites context.
  """

  import Ecto.Query, warn: false
  alias Fav.Repo

  alias Fav.Favourites.Favourite
  alias Fav.{Movies, Users}

  @doc """
  Returns the list of favourites.

  ## Examples

      iex> list_favourites()
      [%Favourite{}, ...]

  """
  def list_favourites do
    Repo.all(Favourite)
  end

  @doc """
  Gets a single favourite.

  Raises `Ecto.NoResultsError` if the Favourite does not exist.

  ## Examples

      iex> get_favourite!(123)
      %Favourite{}

      iex> get_favourite!(456)
      ** (Ecto.NoResultsError)

  """
  def get_favourite!(id), do: Repo.get!(Favourite, id)

  @doc """
  Creates a favourite.

  ## Examples

      iex> create_favourite(%{field: value})
      {:ok, %Favourite{}}

      iex> create_favourite(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_favourite(%{"movie_id" => movie_id, "user_id" => user_id}) do
    %Favourite{movie: Movies.get_movie!(movie_id), user: Users.get_user!(user_id)}
    |> Favourite.changeset(%{})
    |> Repo.insert()
  end

  def create_favourite(_) do
    {:error, :invalid}
  end

  @doc """
  Updates a favourite.

  ## Examples

      iex> update_favourite(favourite, %{field: new_value})
      {:ok, %Favourite{}}

      iex> update_favourite(favourite, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_favourite(%Favourite{} = favourite, attrs) do
    favourite
    |> Favourite.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a favourite.

  ## Examples

      iex> delete_favourite(favourite)
      {:ok, %Favourite{}}

      iex> delete_favourite(favourite)
      {:error, %Ecto.Changeset{}}

  """
  def delete_favourite(%Favourite{} = favourite) do
    Repo.delete(favourite)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking favourite changes.

  ## Examples

      iex> change_favourite(favourite)
      %Ecto.Changeset{data: %Favourite{}}

  """
  def change_favourite(%Favourite{} = favourite, attrs \\ %{}) do
    Favourite.changeset(favourite, attrs)
  end

  def get_favourite_by_movie_and_user!(movie_id, user_id) do
    from(f in Favourite, where: f.movie_id == ^movie_id, where: f.user_id == ^user_id)
    |> Repo.one!()
  end
end
