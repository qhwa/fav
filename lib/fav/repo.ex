defmodule Fav.Repo do
  use Ecto.Repo,
    otp_app: :fav,
    adapter: Ecto.Adapters.Postgres
end
