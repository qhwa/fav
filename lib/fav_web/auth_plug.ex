defmodule FavWeb.AuthPlug do
  import Plug.Conn

  def init(opts), do: opts

  def call(conn, _opts) do
    case authroize!(conn) do
      {:ok, conn} ->
        conn

      _ ->
        conn
        |> put_resp_content_type("application/json")
        |> send_resp(403, ~s'{"success": false, "message": "please login"}')
        |> halt()
    end
  end

  defp authroize!(%{params: %{"user_id" => user_id}} = conn) do
    {:ok, assign(conn, :user, Fav.Users.get_user!(user_id))}
  end

  defp authroize!(_conn) do
    {:error, :unauth}
  end
end
