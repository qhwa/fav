defmodule FavWeb.FavouriteView do
  use FavWeb, :view
  alias FavWeb.FavouriteView

  def render("index.json", %{favourites: favourites}) do
    %{success: true, data: render_many(favourites, FavouriteView, "favourite.json")}
  end

  def render("show.json", %{favourite: favourite}) do
    %{success: true, data: render_one(favourite, FavouriteView, "favourite.json")}
  end

  def render("favourite.json", %{favourite: favourite}) do
    %{movie_id: favourite.movie_id}
  end
end
