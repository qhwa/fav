defmodule FavWeb.FavouriteController do
  use FavWeb, :controller

  alias Fav.Favourites
  alias Fav.Favourites.Favourite

  action_fallback FavWeb.FallbackController

  plug FavWeb.AuthPlug

  def index(conn, _params) do
    favourites = Favourites.list_favourites()
    render(conn, "index.json", favourites: favourites)
  end

  def create(conn, %{"favourite" => %{"movie_id" => _movie_id, "user_id" => _user_id} = params}) do
    with {:ok, %Favourite{} = favourite} <- Favourites.create_favourite(params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.favourite_path(conn, :show, favourite))
      |> render("show.json", favourite: favourite)
    end
  end

  def create(_conn, _) do
    {:error, "invalid parameters"}
  end

  def delete(conn, %{"movie_id" => movie_id, "user_id" => user_id}) do
    with %Favourite{} = favourite <-
           Favourites.get_favourite_by_movie_and_user!(movie_id, user_id),
         {:ok, %Favourite{}} <- Favourites.delete_favourite(favourite) do
      conn
      |> put_resp_content_type("application/json")
      |> send_resp(204, ~s[{"success": true}])
    end
  end
end
