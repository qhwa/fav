# Favourites

## Setup and run

~~~sh
mix do deps.get, ecto.setup
mix phx.server
~~~

Visit: http://127.0.0.1:4000/api/favourites

## Design

### Common basis

- JSON API
- application/json
- authorization: user_id from paramters
- only accept movie id at this time
- responding format:
  - `{"success": true, data: ...}`
  - `{"success": false, message: "reason...", error_code: ...}`
  - status_code: RESTful

### Create

POST /favourites

#### Parameters:

- `movie_id` : id of the target movie
- `user_id` : id of the current_user

#### Returns

```json
{"success": true}
```


### List

GET /favourites

#### Parameters:

- `user_id` : id of the current_user
- `page` : current page number
- `per_page` : favourites count per page

#### Returns

```json
{"success": true, "data": [{"id": id, other_movie_data}, ...]}
```

### Delete

DELETE /favourites

#### Parameters:

- `user_id` : id of the current_user
- `movie_id`: string

#### Returns

```json
{"success": true}
```

## Features

* [x] integration and unit test
* [x] runnable http server
* [ ] linting with credo
* [ ] static code dialyzing
