defmodule FavWeb.FavouriteControllerTest do
  use FavWeb.ConnCase
  import Fav.Factory

  alias Fav.Favourites

  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "error when no user_id provided", %{conn: conn} do
      conn = get(conn, Routes.favourite_path(conn, :index))
      assert json_response(conn, 403)["message"] == "please login"
      refute json_response(conn, 403)["success"]
    end

    test "lists none favourites when no favourites available", %{conn: conn} do
      user = insert(:user)
      conn = get(conn, Routes.favourite_path(conn, :index), user_id: user.id)
      assert json_response(conn, 200)["data"] == []
    end

    test "lists all favourites", %{conn: conn} do
      user = insert(:user)
      for _ <- 1..100, do: insert(:favourite, user: user)

      conn = get(conn, Routes.favourite_path(conn, :index), user_id: user.id)
      data = json_response(conn, 200)["data"]
      assert length(data) == 100
    end
  end

  describe "create favourite" do
    test "renders favourite when data is valid", %{conn: conn} do
      movie = insert(:movie)
      user = insert(:user)
      create_attrs = %{movie_id: movie.id, user_id: user.id}

      conn =
        post(conn, Routes.favourite_path(conn, :create), favourite: create_attrs, user_id: user.id)

      assert json_response(conn, 201)["success"]
      assert %{"movie_id" => id} = json_response(conn, 201)["data"]
      assert id
      assert Favourites.get_favourite_by_movie_and_user!(id, user.id)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      user = insert(:user)

      conn =
        post(conn, Routes.favourite_path(conn, :create),
          favourite: @invalid_attrs,
          user_id: user.id
        )

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete favourite" do
    test "deletes chosen favourite", %{conn: conn} do
      favourite = insert(:favourite)

      conn =
        delete(
          conn,
          Routes.favourite_path(conn, :delete, favourite.movie_id),
          user_id: favourite.user_id
        )

      assert json_response(conn, 204)["success"]
    end

    test "not logged in", %{conn: conn} do
      favourite = insert(:favourite)

      conn =
        delete(
          conn,
          Routes.favourite_path(conn, :delete, favourite.movie_id)
        )

      refute json_response(conn, 403)["success"]
    end
  end
end
