defmodule Fav.FavouritesTest do
  use Fav.DataCase
  import Fav.Factory

  alias Fav.Favourites

  describe "favourites" do
    alias Fav.Favourites.Favourite

    @invalid_attrs %{}

    def favourite_fixture(_attrs \\ %{}) do
      insert(:favourite)
    end

    test "list_favourites/0 returns all favourites" do
      favourite = favourite_fixture()
      assert list_favourites() == [favourite]
    end

    test "get_favourite!/1 returns the favourite with given id" do
      favourite = favourite_fixture()
      assert load_favourite(favourite.id) == favourite
    end

    test "create_favourite/1 with valid data creates a favourite" do
      user = insert(:user)
      movie = insert(:movie)
      attrs = %{"user_id" => user.id, "movie_id" => movie.id}
      assert {:ok, %Favourite{} = favourite} = Favourites.create_favourite(attrs)
    end

    test "create_favourite/1 with invalid data returns error changeset" do
      assert {:error, :invalid} = Favourites.create_favourite(@invalid_attrs)
    end

    test "delete_favourite/1 deletes the favourite" do
      favourite = favourite_fixture()
      assert {:ok, %Favourite{}} = Favourites.delete_favourite(favourite)
      assert_raise Ecto.NoResultsError, fn -> Favourites.get_favourite!(favourite.id) end
    end
  end

  defp list_favourites do
    Favourites.list_favourites()
    |> Fav.Repo.preload([:user, :movie])
  end

  defp load_favourite(id) do
    Favourites.get_favourite!(id)
    |> Fav.Repo.preload([:user, :movie])
  end
end
