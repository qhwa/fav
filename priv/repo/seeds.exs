# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Fav.Repo.insert!(%Fav.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
import Fav.Factory

for user <- ~w[Joe Billy Kitty Mike], do: insert(:user, name: user)
for mov <- ~w[M1 M2 M3], do: insert(:movie, name: mov)
