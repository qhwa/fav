defmodule Fav.Repo.Migrations.CreateFavourites do
  use Ecto.Migration

  def change do
    create table(:favourites) do
      add :movie_id, references(:movies, on_delete: :nothing)
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:favourites, [:movie_id])
    create index(:favourites, [:user_id])
  end
end
